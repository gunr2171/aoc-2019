module Tests

open System
open System.Collections.Generic
open Xunit

let factoryTenStack = [0..9]

[<Fact>]
let ``Deal Info New Stack Examples`` () =
    let actual = Program.dealIntoNewStack 2L 10L
    let expected = 7L
    Assert.Equal(expected, actual)

[<Fact>]
let ``Cut Stack Examples`` () =
    let actual = Program.cutStack 5L 7L 10L
    let expected = 2L
    Assert.Equal(expected, actual)
