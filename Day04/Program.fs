﻿// Learn more about F# at http://fsharp.org

open System
open System.Text.RegularExpressions

//let numberToDigits number = false
   
let (|Regex|_|) pattern input =
     let m = Regex.Match(input, pattern)
     if m.Success then Some(List.tail [ for g in m.Groups -> g.Value ])
     else None

let numberHasDoubleDigitsP1 number =
    Regex.IsMatch(number, @"(\d)\1")

// https://github.com/IgneSapien/AdventOfCode2019/blob/master/Day4/Program.cs#L42
let numberHasDoubleDigitsP2 number =
    let doubleCount = number
                    |> Seq.groupBy id
                    |> Seq.filter (fun (k, g) -> (Seq.length g) = 2)
                    |> Seq.length
    doubleCount > 0

let compareSequences = Seq.compareWith Operators.compare
let sequecesAreSame seq1 seq2 = (compareSequences seq1 seq2 = 0)

let numberDigitsNeverDecrease number =
    let sorted = number |> Seq.sort
    sequecesAreSame number sorted

let testIfNumberIsValidPasswordP1 number =
    numberHasDoubleDigitsP1 number && numberDigitsNeverDecrease number

let testIfNumberIsValidPasswordP2 number =
    numberHasDoubleDigitsP2 number && numberDigitsNeverDecrease number

[<EntryPoint>]
let main argv =
    let startNumber = 136760
    let endNumber = 595730
    let numbers = seq { startNumber..endNumber } 
                |> Seq.map string

    numbers
        |> Seq.filter testIfNumberIsValidPasswordP1
        |> Seq.length
        |> printfn "Part 1: %d"

    numbers
        |> Seq.filter testIfNumberIsValidPasswordP2
        |> Seq.length
        |> printfn "Part 2: %d"
    
    0 // return an integer exit code
