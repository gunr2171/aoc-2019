﻿// Learn more about F# at http://fsharp.org

open System

type instruction = { Parent:string; Child:string }

let readInputFile =
    System.IO.File.ReadAllLines("Input.txt")

let parseEntry (entry : string) =
    let segments = entry.Split(')')
    { Parent = segments.[0]; Child = segments.[1] }

let addChild parent child (table:Map<string, string list>) =
    let children = 
        match Map.tryFind parent table with
        | Some x -> x @ [ child ]
        | None -> [ child ]

    Map.add parent children table

let rec insertIntoOrbitMap instructions = 
  match instructions with 
  | e::rest -> insertIntoOrbitMap rest |> addChild e.Parent e.Child
  | [] -> Map.empty

  
let rec countParents (level: int) (startSymbol: string) (orbitMap: Map<string, string list>) =
    match Map.tryFind startSymbol orbitMap with
    | Some children -> let childSum = children |> List.map (fun child -> countParents (level + 1) child orbitMap) |> List.sum
                       childSum + level
                       // level + (List.map (fun child -> countParents (level + 1) child orbitMap) |> List.sum)
    | None -> level

[<EntryPoint>]
let main argv =
    readInputFile 
        |> Array.map parseEntry
        |> Array.toList
        |> insertIntoOrbitMap
        |> countParents 0 "COM"
        |> printfn "Part 1: %d"

    0 // return an integer exit code

