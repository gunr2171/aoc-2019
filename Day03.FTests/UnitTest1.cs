using System;
using Xunit;
using FluentAssertions;
using System.Linq;

namespace Day03.FTests
{
    public class UnitTest1
    {
        [Fact]
        public void CorrectPointsExample1()
        {
            var line1 = new[] { "R8", "U5", "L5", "D3" };
            var line2 = new[] { "U7", "R6", "D4", "L4"};

            var matchingPoints = Program.findMatchingPointsFromInstructions(line1, line2);

            var expectedPoints = new[]
            {
                new Program.point(3,3,20),
                new Program.point(6,5,15)
            };

            matchingPoints.ToList().Should().BeEquivalentTo(expectedPoints);
        }

        [Fact]
        public void FullExample1()
        {
            var line1 = new[] { "R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72" };
            var line2 = new[] { "U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83" };

            var matchingPoints = Program.findMatchingPointsFromInstructions(line1, line2);
            var bestPointDistance = Program.bestMatchingPointDistance(matchingPoints);

            bestPointDistance.Should().Be(159);
        }

        [Fact]
        public void FullExample2()
        {
            var line1 = new[] { "R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51" };
            var line2 = new[] { "U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7" };

            var matchingPoints = Program.findMatchingPointsFromInstructions(line1, line2);
            var bestPointDistance = Program.bestMatchingPointDistance(matchingPoints);

            bestPointDistance.Should().Be(135);
        }
    }
}
