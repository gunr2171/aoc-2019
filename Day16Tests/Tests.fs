module Tests

open System
open Xunit

[<Fact>]
let ``Check Lengthen with 3`` () =
    let expected = [1;1;1;2;2;2;3;3;3]
    let actual = Program.lengthen 3 [1;2;3]
    Assert.Equal<Collections.Generic.IEnumerable<int>>(expected, actual)

[<Fact>]
let ``Example 1 Round 1 Position 1`` () =
    let input = [ 1;2;3;4;5;6;7;8 ]
    let expected = 4
    let actual = Program.processPhasePosition input 1
    Assert.Equal(expected, actual)

[<Fact>]
let ``Example 1 Round 1 Position 2`` () =
    let input = [ 1;2;3;4;5;6;7;8 ]
    let expected = 8
    let actual = Program.processPhasePosition input 2
    Assert.Equal(expected, actual)

[<Fact>]
let ``Example 1 Round 1 Position 8`` () =
    let input = [ 1;2;3;4;5;6;7;8 ]
    let expected = 8
    let actual = Program.processPhasePosition input 8
    Assert.Equal(expected, actual)

[<Fact>]
let ``Example 1 Round 1 Full`` () =
    let input = [ 1;2;3;4;5;6;7;8 ]
    let expected = [ 4;8;2;2;6;1;5;8 ]
    let actual = Program.processPhase input
    Assert.Equal<Collections.Generic.IEnumerable<int>>(expected, actual)

[<Fact>]
let ``Example 1 Round 2 Full`` () =
    let input = [ 4;8;2;2;6;1;5;8 ]
    let expected = [ 3;4;0;4;0;4;3;8 ]
    let actual = Program.processPhase input
    Assert.Equal<Collections.Generic.IEnumerable<int>>(expected, actual)

[<Fact>]
let ``Full Example 1`` () =
    let input = [ 8;0;8;7;1;2;2;4;5;8;5;9;1;4;5;4;6;6;1;9;0;8;3;2;1;8;6;4;5;5;9;5 ]
    let expected = [ 2;4;1;7;6;1;7;6 ]
    let actual = Program.processNumPhases 100 input
               |> Seq.take 8
    Assert.Equal<Collections.Generic.IEnumerable<int>>(expected, actual)

[<Fact>]
let ``Run 4 Phases`` () =
    let input = [ 1;2;3;4;5;6;7;8 ]
    let expected = [ 0;1;0;2;9;4;9;8 ]
    let actual = Program.processNumPhases 4 input
    Assert.Equal<Collections.Generic.IEnumerable<int>>(expected, actual)
