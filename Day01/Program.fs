﻿// Learn more about F# at http://fsharp.org

open System

let readInputFile =
    System.IO.File.ReadAllLines("Inputs.txt")
    |> Array.map int

let fuleNeededForModule mass =
    (mass / 3) - 2

let rec fuleNeededForModuleRec mass = 
    let fuleNeeded = fuleNeededForModule mass
    match fuleNeeded with
    | (fule) when fule > 0 -> fuleNeeded + fuleNeededForModuleRec fuleNeeded
    | _ -> 0

[<EntryPoint>]
let main argv =
    let inputs = readInputFile

    let part1Answer = inputs |> Seq.map fuleNeededForModule |> Seq.sum
    printfn "Part 1: %d" part1Answer

    let part2Answer = inputs |> Seq.map fuleNeededForModuleRec |> Seq.sum
    printfn "Part 2: %d" part2Answer
    0 // return an integer exit code
