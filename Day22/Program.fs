﻿// Learn more about F# at http://fsharp.org

open System

// mod(x, m) = x + mk
// newStack(x,m) = (-x-1) + mk
// cut(x,y,m) = (x - y) + mk
// deal(x,y,m) = (x * y) + mk

// 2 |> newStack 10 |> cut 5 10
// ((-2 - 1) + 10k1) - 5) + 10k2
// -8 mod 10 = 2

// x |> newStack m |> cut y m
// ((-x - 1) + mk1) - y) + mk2
// (-1 - x - y1) % m

// |> deal
// (((-x - 1)) - y1)) * y2
// -xy2 - y1y2 - y1y2 
// C1x + C2
// ((C1x + C2) * C3) * C4
// foo x = ((C1x + C2) * C1 + C2) * C1 + C2 ...
// (C1x)**Z + C2*C1**Z - C2*C1**(Z-1) + C2*C1**(Z-2) ...

// C2*C1**Z - C2*C1**(Z-1) + C2*C1**(Z-2) = z
// Zlog(C2*C1) - (Z-1)log(C2*C1) ... = log(z)
// (Z - (Z-1) - (Z-2) - (Z-3) ...)log(C2*C1) = log(z)
// (Z - Z*(Z-1) - Z!!) = log(z)

// x |> foo |> foo |> foo

// x |> f >> g = g(f(x))
// x |> f . g = f(g(x))

// http://gettingsharper.de/2012/02/28/how-to-implement-a-mathematically-correct-modulus-operator-in-f/
let modulo n m = ((n % m) + m) % m

// reverse the stack. You get the "mirror opposite" index
let dealIntoNewStack currentIndex totalCards = modulo (-currentIndex - 1L) totalCards // totalCards - currentIndex - 1L

// if your index is after the TrueCut amount, reduce the index by the TrueCut amount
// if your index is before the TrueCut amount, add all the positions that were after the TrueCut amount
let cutStack cutAmount currentIndex totalCards =
    modulo (currentIndex - cutAmount) totalCards

let dealWithIncrement incrementAmount currentIndex totalCards =
    (incrementAmount * currentIndex) % totalCards

let inputFileLines = System.IO.File.ReadAllLines("Input.txt") |> Array.toList

let getParameterFromInstruction (instruction : string) = instruction.Split(' ') |> Array.last |> int64

let (|Prefix|_|) (p:string) (s:string) =
    if s.StartsWith(p) then
        Some(s.Substring(p.Length))
    else
        None

let runInstruction instruction currentPosition totalCards =
    match instruction with 
    | Prefix "deal into" v -> dealIntoNewStack currentPosition totalCards
    | Prefix "cut" v -> let amount = getParameterFromInstruction instruction
                        cutStack amount currentPosition totalCards
    | Prefix "deal with" v -> let amount = getParameterFromInstruction instruction
                              dealWithIncrement amount currentPosition totalCards
    | _ -> failwith "bad instruction"

let rec runAllInstructions instructions currentPosition totalCards =
    match instructions with
    | current::tail -> let newPosition = runInstruction current currentPosition totalCards
                       runAllInstructions tail newPosition totalCards
    | [] -> currentPosition

let rec runPart instructions trackedPosition totalCards instructionLoopTimes =
    match instructionLoopTimes with 
    | x when x = 0L -> trackedPosition
    | _ -> let newPosition = runAllInstructions instructions trackedPosition totalCards
           runPart instructions newPosition totalCards (instructionLoopTimes - 1L)

[<EntryPoint>]
let main argv =
    printfn "Hello World from F#!"

    runPart inputFileLines 2019L 10_007L 1L |> printfn "Part 1: %A"
    runPart inputFileLines 2019L 10_007L 2L |> printfn "Part 1B: %A"
    runPart inputFileLines 2019L 10_007L 3L |> printfn "Part 1C: %A"
    runPart inputFileLines 2019L 10_007L 4L |> printfn "Part 1D: %A"
    runPart inputFileLines 2019L 10_007L 5L |> printfn "Part 1E: %A"
    //runPart inputFileLines 2020L 119_315_717_514_047L 101_741_582_076_661L |> printfn "Part 2: %A"

    //let smallFactoryCards = [0..10_006]
    //let smallShuffledCards = shuffleCards smallFactoryCards
    //smallShuffledCards |> List.findIndex is2019 |> printfn "Part 1: %d"

    //let largeFactoryCards = [0..119315717514046I]
    //let largeShuffledCards = shuffleCards largeFactoryCards
    //largeShuffledCards |> List.findIndex is2019 |> printfn "Part 1: %d"

    0 // return an integer exit code
