﻿using System;
using System.Linq;

namespace Day02CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = new [] { 
               1, 0, 0, 3,
               1, 1, 2, 3,
               1, 3, 4, 3,
               1, 5, 0, 3,
               2, 10, 1, 19,
               1, 19, 5, 23,
               1, 23, 9, 27,
               2, 27, 6, 31,
               1, 31, 6, 35,
               2, 35, 9, 39,
               1, 6, 39, 43,
               2, 10, 43, 47,
               1, 47, 9, 51,
               1, 51, 6, 55,
               1, 55, 6, 59,
               2, 59, 10, 63,
               1, 6, 63, 67,
               2, 6, 67, 71,
               1, 71, 5, 75,
               2, 13, 75, 79,
               1, 10, 79, 83,
               1, 5, 83, 87,
               2, 87, 10, 91,
               1, 5, 91, 95,
               2, 95, 6, 99,
               1, 99, 6, 103,
               2, 103, 6, 107,
               2, 107, 9, 111,
               1, 111, 5, 115,
               1, 115, 6, 119,
               2, 6, 119, 123,
               1, 5, 123, 127,
               1, 127, 13, 131,
               1, 2, 131, 135,
               1, 135, 10, 0,
               99, 
               2, 14, 0, 0 };

            var part1Answer = RunProgram(input.ToArray(), 12, 2);
            Console.WriteLine($"Part 1: {part1Answer}");

            var possibleVal1 = Enumerable.Range(0, 100);
            var possibleVal2 = Enumerable.Range(0, 100);

            var expectedAnswer = 19690720;

            var part2Answers = from val1 in possibleVal1
                           from val2 in possibleVal2
                           where RunProgram(input.ToArray(), val1, val2) == expectedAnswer
                           select 100 * val1 + val2;

            Console.WriteLine($"Part 2: {part2Answers.Single()}");
        }

        private static int RunProgram(int[] inputs, int val1, int val2)
        {
            // alteration
            inputs[1] = val1;
            inputs[2] = val2;

            for (var currentIndex = 0; currentIndex < inputs.Length; currentIndex += 4)
            {
                var optCode = inputs[currentIndex];

                if (optCode == 99)
                    break;

                var param1 = inputs[currentIndex + 1];
                var param2 = inputs[currentIndex + 2];
                var indexToStore = inputs[currentIndex + 3];

                int valueToStore;

                if (optCode == 1)
                    valueToStore = inputs[param1] + inputs[param2];
                else if (optCode == 2)
                    valueToStore = inputs[param1] * inputs[param2];
                else
                    throw new Exception();

                inputs[indexToStore] = valueToStore;
            }

            return inputs[0];
        }
    }
}
